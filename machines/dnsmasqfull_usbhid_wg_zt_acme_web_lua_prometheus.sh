#!/usr/bin/env sh

# no DHCPv6 client (does not affect DHCPv4 client udhcpc which is provided by busybox)
PACKAGES="${PACKAGES} -odhcp6c"

# dnsmasq-full DHCPv4/v6 server, local DNS and DNS authoritative server (instead of dnsmasq/IPv4, odhcpd-ipv6only/IPv6, and nsd)
# ipset to add IPs of particular resolved domains into an IP set and route them (later) via a VPN connection
PACKAGES="${PACKAGES} -dnsmasq -odhcpd-ipv6only dnsmasq-full ipset"

# DNS-over-HTTPS (DoH) proxy, takes 312128 bytes (https-dns-proxy, libcurl4, libmbedtls)
#PACKAGES="${PACKAGES} https-dns-proxy"

# DNS-over-TLS (DoT) proxy, takes 214664 bytes (stubby, getdns, libyaml) and has lower overhead, see https://en.blog.nic.cz/2020/11/25/encrypted-dns-in-knot-resolver-dot-and-doh/
# DISABLED: slow and sometimes a series of timeouts due to TLS failure
#PACKAGES="${PACKAGES} stubby"

# USB Storage with DOS filesystem, e.g., for Jablotron JA-100
# DISABLED: it will fit (the overall size of the image would be ok), however, it is not needed as the JA-100 storage contains just unimportant files
#PACKAGES="${PACKAGES} kmod-usb-storage kmod-fs-vfat dosfstools block-mount"

# USB-over-IP to forward Jablotron to Home Assistant on an external server
# NOTE: kmod-usb-ohci/uhci (USB 1.1) does not work here, must use kmod-usb-ehci (USB 2.0) to support the USB chip of the device
# DISABLED: there is a bug in vhci-hcd kernel module for an USPIP client, so Jablotron cannot be forwarded by usbip
#PACKAGES="${PACKAGES} kmod-usb-ehci usbip-client usbip-server"

# USB HID for Jablotron JA-100 forwarded by socat and accessed by Python socket module from a HA plugin
PACKAGES="${PACKAGES} kmod-usb-hid"

# 6in4 Tunnel, e.g. henet
PACKAGES="${PACKAGES} 6in4"

# WireGuard, e.g., for vpsFree IPv6 Tunnel
PACKAGES="${PACKAGES} wireguard-tools"

# L2TP/IPsec VPN Tunnel
PACKAGES="${PACKAGES} xl2tpd"

# ZeroTier private VPN
PACKAGES="${PACKAGES} zerotier"

# Let’s Encrypt (ACME)
PACKAGES="${PACKAGES} acme"

# Replace WolfSSL by OpenSSL as openssl is already required, e.g., by openssh-sftp-server
# libustream-ssl is required by uhttpd
PACKAGES="${PACKAGES} libustream-openssl -libustream-wolfssl wpad-basic-openssl -wpad-basic-wolfssl"

# Web Lua applications
PACKAGES="${PACKAGES} uhttpd-mod-lua luasec lua-md5 libuci-lua"

# SFTP server to mount via GIO
PACKAGES="${PACKAGES} openssh-sftp-server"

# Prometeus Node Exporter for reporting metrics to Prometeus/Grafana
PACKAGES="${PACKAGES} prometheus-node-exporter-lua-nat_traffic prometheus-node-exporter-lua-netstat prometheus-node-exporter-lua-openwrt prometheus-node-exporter-lua-uci_dhcp_host prometheus-node-exporter-lua-wifi prometheus-node-exporter-lua-wifi_stations"

export PROFILE=tplink_tl-wdr3600-v1
export SUBTARGET=generic
export PACKAGES
