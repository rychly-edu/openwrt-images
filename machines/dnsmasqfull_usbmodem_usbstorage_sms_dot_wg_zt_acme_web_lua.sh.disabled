#!/usr/bin/env sh

# no DHCPv6 client (does not affect DHCPv4 client udhcpc which is provided by busybox)
PACKAGES="${PACKAGES} -odhcp6c"

# dnsmasq-full DHCPv4/v6 server, local DNS and DNS authoritative server (instead of dnsmasq/IPv4, odhcpd-ipv6only/IPv6, and nsd)
# ipset to add IPs of particular resolved domains into an IP set and route them (later) via a VPN connection
PACKAGES="${PACKAGES} -dnsmasq -odhcpd-ipv6only dnsmasq-full ipset"

# DNS-over-HTTPS (DoH) proxy, takes 312128 bytes (https-dns-proxy, libcurl4, libmbedtls)
#PACKAGES="${PACKAGES} https-dns-proxy"

# DNS-over-TLS (DoT) proxy, takes 214664 bytes (stubby, getdns, libyaml) and has lower overhead, see https://en.blog.nic.cz/2020/11/25/encrypted-dns-in-knot-resolver-dot-and-doh/
PACKAGES="${PACKAGES} stubby"

# USB Storage with DOS and F2FS filesystem
PACKAGES="${PACKAGES} kmod-usb-storage kmod-fs-f2fs f2fs-tools kmod-fs-vfat dosfstools block-mount"

# Mobile USB Dongle, SMS receiver/sender
PACKAGES="${PACKAGES} kmod-usb-serial-option usb-modeswitch smstools3"

# 6in4 Tunnel, e.g. henet
PACKAGES="${PACKAGES} 6in4"

# WireGuard, e.g., for vpsFree IPv6 Tunnel
PACKAGES="${PACKAGES} wireguard"

# Tor and I2P (Invisible Internet Protocol)
# REMOVED: for better security these services will be used locally
##PACKAGES="${PACKAGES} tor i2pd"

# L2TP/IPsec VPN Tunnel
PACKAGES="${PACKAGES} xl2tpd"

# ZeroTier private VPN
PACKAGES="${PACKAGES} zerotier"

# Let’s Encrypt (ACME)
# NOTE: The ordering of packages is IMPORTANT, as acme requires gnu-wget which is provided by wget-nossl (primary) and wget (secondary), so wget-nossl is installed automatically if there is no wget
PACKAGES="${PACKAGES} -wget-nossl wget acme"

# Web Server (required for ACME domain validation)
PACKAGES="${PACKAGES} libuhttpd-openssl"

# Web Lua applications
PACKAGES="${PACKAGES} uhttpd-mod-lua luasec luafilesystem json4lua lua-md5 libuci-lua"

# Email Server
# REMOVED: we will us a mail-server in an external VPS which will post SMS messages via a REST API to the local web-server
##PACKAGES="${PACKAGES} xmail"

# SFTP server to mount via GIO
PACKAGES="${PACKAGES} openssh-sftp-server"

export PROFILE=tplink_tl-wdr3600-v1
export SUBTARGET=generic
export PACKAGES
