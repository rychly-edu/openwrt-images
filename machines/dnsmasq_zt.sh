#!/usr/bin/env sh

# see https://openwrt.org/docs/guide-user/additional-software/saving_space

# remove PPPoE
PACKAGES="${PACKAGES} -ppp -ppp-mod-pppoe"

# remove IPv6
PACKAGES="${PACKAGES} -ip6tables -odhcp6c -kmod-ipv6 -kmod-ip6tables -odhcpd-ipv6only"

# remove DHCP server
PACKAGES="${PACKAGES} -odhcpd"

# ZeroTier private VPN
PACKAGES="${PACKAGES} zerotier"

export PROFILE=tplink_tl-wr841-v8
export SUBTARGET=tiny
export PACKAGES
