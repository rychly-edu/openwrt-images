#!/usr/bin/env sh

# Unbound DNS resolver and odhcpd DHCPv4/v6 server (instead of dnsmasq/IPv4 and odhcpd/IPv6)
# see https://github.com/openwrt/packages/blob/master/net/unbound/files/README.md#unbound-and-odhcpd
PACKAGES="${PACKAGES} -odhcpd-ipv6only odhcpd"
PACKAGES="${PACKAGES} -dnsmasq unbound-control"

# DNS authoritative server
PACKAGES="${PACKAGES} nsd"

# USB Storage with ext4 filesystem
# REMOVED-19.07.0-1: no longer using USB flash disk and trying to cope with error: images are too big by 602473 bytes
#PACKAGES="${PACKAGES} kmod-usb-storage kmod-fs-ext4 e2fsprogs block-mount"

# Mobile USB Dongle, SMS receiver/sender
PACKAGES="${PACKAGES} kmod-usb-serial-option usb-modeswitch smstools3"

# 6in4 Tunnel, e.g. henet
PACKAGES="${PACKAGES} 6in4"

# OpenVPN, e.g., for vpsFree IPv6 Tunnel
PACKAGES="${PACKAGES} openvpn-openssl"

# Tor
PACKAGES="${PACKAGES} tor"

# L2TP/IPsec VPN Tunnel
PACKAGES="${PACKAGES} xl2tpd"

# Let’s Encrypt (ACME)
# NOTE: The ordering of packages is IMPORTANT, as acme requires gnu-wget which is provided by wget-nossl (primary) and wget (secondary), so wget-nossl is installed automatically if there is no wget
PACKAGES="${PACKAGES} -wget-nossl wget acme"

# Web Server (required for ACME domain validation)
PACKAGES="${PACKAGES} libuhttpd-openssl"

# Web Lua applications
PACKAGES="${PACKAGES} uhttpd-mod-lua luasec luafilesystem json4lua lua-md5 libuci-lua"

# Email Server
PACKAGES="${PACKAGES} xmail"

# SFTP server to mount via GIO
PACKAGES="${PACKAGES} openssh-sftp-server"

export PROFILE=tplink_tl-wdr3600-v1
export SUBTARGET=generic
export PACKAGES
